#!/bin/bash
echo "Mise à jour APT"
sudo apt-get update > /dev/null
sudo apt-get upgrade -y > /dev/null
echo "Installation de Python3 et PIP3"
sudo apt-get -qq install -y python3 python3-pip > /dev/null
echo "Installation de Docker"
sudo apt-get -qq install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common > /dev/null
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
arch=$(uname -m)
if [[ "$arch" =~ ^x86.* ]]; then arch=amd64; fi
if [[ "$arch" =~ ^arm.* ]]; then arch=arm64; fi
sudo add-apt-repository "deb [arch=$arch] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
sudo apt-get update  > /dev/null
sudo apt-get -qq install -y docker-ce docker-ce-cli containerd.io  > /dev/null
echo "Installation de Docker-Compose"
sudo pip3 -q install docker-compose
sudo addgroup docker $USER
sudo usermod -aG docker $USER
echo "Installation terminée, veuillez relancer la session (déconnexion et reconnexion)"
