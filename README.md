# Description
Ceci est un script qui permet d'installer Python, Pip, Docker et Docker-Compose sur une nouvelle machine

# Prérequis 
La commande sudo doit être utilisable par l'utilsateur
```
su - 
apt-get install sudo -y
usermod -aG sudo [login]
```

Remplacez [login] par le login de votre session
Il faut essuite se reconnecter pour appliquer les modifications


# Utilisation

Télécharger le script : 
```
wget https://gitlab.com/boced66/install-docker-script/-/raw/master/script-install.sh
chmod +x script-install.sh
./script-install.sh
```

Ou en une ligne : 

`wget https://gitlab.com/boced66/install-docker-script/-/raw/master/script-install.sh && chmod +x script-install.sh && ./script-install.sh`
